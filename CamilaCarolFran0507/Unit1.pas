unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Unit2, Vcl.StdCtrls, Vcl.DBCtrls,
  Vcl.ExtCtrls, Unit3;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    DBLookupListBox1: TDBLookupListBox;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    DBText8: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
Form3 := TForm3.Create(Application);
Form3.Show;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
Form3 := TForm3.Create(Application);
Form3.Show;
Form3.DBEdit1.Clear;
Form3.DBEdit2.Clear;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  DataModule2.FDQueryPokemon.Delete;
end;

end.
