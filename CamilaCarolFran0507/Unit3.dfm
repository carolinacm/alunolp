object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 275
  ClientWidth = 307
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 48
    Top = 56
    Width = 31
    Height = 13
    Caption = 'Nome:'
  end
  object Label2: TLabel
    Left = 48
    Top = 96
    Width = 50
    Height = 13
    Caption = 'Treinador:'
  end
  object Label3: TLabel
    Left = 48
    Top = 139
    Width = 27
    Height = 13
    Caption = 'N'#237'vel:'
  end
  object DBEdit1: TDBEdit
    Left = 136
    Top = 53
    Width = 121
    Height = 21
    DataField = 'nome'
    DataSource = DataModule2.DataSourcePokemon
    TabOrder = 0
  end
  object DBEdit2: TDBEdit
    Left = 136
    Top = 136
    Width = 121
    Height = 21
    DataField = 'nivel'
    DataSource = DataModule2.DataSourcePokemon
    TabOrder = 1
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 136
    Top = 96
    Width = 121
    Height = 21
    DataField = 'id_treinador'
    DataSource = DataModule2.DataSourcePokemon
    KeyField = 'id_treinador'
    ListField = 'nome'
    ListSource = DataModule2.DataSourceTreinador
    TabOrder = 2
  end
  object Button1: TButton
    Left = 48
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 182
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 4
    OnClick = Button2Click
  end
end
