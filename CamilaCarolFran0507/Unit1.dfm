object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 392
  ClientWidth = 642
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 215
    Top = 8
    Width = 258
    Height = 217
    TabOrder = 0
    object DBText5: TDBText
      Left = 176
      Top = 40
      Width = 65
      Height = 17
      DataField = 'id_pokemon'
      DataSource = DataModule2.DataSourcePokemon
    end
    object DBText6: TDBText
      Left = 176
      Top = 80
      Width = 65
      Height = 17
      DataField = 'id_treinador'
      DataSource = DataModule2.DataSourcePokemon
    end
    object DBText7: TDBText
      Left = 176
      Top = 120
      Width = 65
      Height = 17
      DataField = 'nivel'
      DataSource = DataModule2.DataSourcePokemon
    end
    object DBText8: TDBText
      Left = 176
      Top = 160
      Width = 65
      Height = 17
      DataField = 'nome'
      DataSource = DataModule2.DataSourcePokemon
    end
    object Label1: TLabel
      Left = 40
      Top = 40
      Width = 107
      Height = 13
      Caption = 'Identificador Pok'#233'mon'
    end
    object Label2: TLabel
      Left = 40
      Top = 80
      Width = 46
      Height = 13
      Caption = 'Treinador'
    end
    object Label3: TLabel
      Left = 40
      Top = 120
      Width = 23
      Height = 13
      Caption = 'N'#237'vel'
    end
    object Label4: TLabel
      Left = 40
      Top = 160
      Width = 27
      Height = 13
      Caption = 'Nome'
    end
  end
  object Button1: TButton
    Left = 8
    Top = 241
    Width = 193
    Height = 25
    Caption = 'Editar'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 8
    Top = 272
    Width = 193
    Height = 25
    Caption = 'Inserir'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 8
    Top = 303
    Width = 193
    Height = 25
    Caption = 'Excluir'
    TabOrder = 3
    OnClick = Button3Click
  end
  object DBLookupListBox1: TDBLookupListBox
    Left = 8
    Top = 8
    Width = 193
    Height = 212
    KeyField = 'id_pokemon'
    ListField = 'nome'
    ListSource = DataModule2.DataSourcePokemon
    TabOrder = 4
  end
end
