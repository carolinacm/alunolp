object DataModule2: TDataModule2
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 291
  Width = 573
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 'C:\EasyPHP-DevServer-14.1VC9\binaries\mysql\lib\libmysql.dll'
    Left = 216
    Top = 104
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=pokemon'
      'User_Name=root'
      'Password=cedup'
      'DriverID=MySQL')
    Connected = True
    Left = 296
    Top = 104
  end
  object FDQueryPokemon: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'SELECT * FROM pokemon')
    Left = 384
    Top = 104
  end
  object DataSourcePokemon: TDataSource
    DataSet = FDQueryPokemon
    Left = 480
    Top = 104
  end
  object FDQueryTreinador: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'SELECT * FROM treinador')
    Left = 384
    Top = 168
  end
  object DataSourceTreinador: TDataSource
    DataSet = FDQueryTreinador
    Left = 480
    Top = 168
  end
end
