object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 463
  ClientWidth = 509
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 56
    Top = 336
    Width = 75
    Height = 25
    Caption = 'Baladinha'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 185
    Height = 313
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
  end
  object Button2: TButton
    Left = 215
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Length'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 215
    Top = 47
    Width = 75
    Height = 25
    Caption = 'Contains'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 215
    Top = 78
    Width = 75
    Height = 25
    Caption = 'Trim'
    TabOrder = 4
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 215
    Top = 109
    Width = 75
    Height = 25
    Caption = 'Lowercase'
    TabOrder = 5
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 215
    Top = 140
    Width = 75
    Height = 25
    Caption = 'Uppercase'
    TabOrder = 6
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 215
    Top = 171
    Width = 75
    Height = 25
    Caption = 'Replace'
    TabOrder = 7
    OnClick = Button7Click
  end
  object Memo2: TMemo
    Left = 308
    Top = 8
    Width = 185
    Height = 313
    Lines.Strings = (
      'Memo2')
    TabOrder = 8
  end
end
