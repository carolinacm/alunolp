unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, StrUtils;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Memo2: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
Memo1.Color := Random(999999);
end;

procedure TForm1.Button2Click(Sender: TObject);
var contador : string;
begin
  contador:= Memo1.Text;
  Memo2.Text:= contador.Length.ToString;
end;

procedure TForm1.Button3Click(Sender: TObject);
var containss : AnsiString;
begin
containss := Memo1.Text;
if AnsiContainsStr(containss, 's')
then Memo2.Text :=('True')
else
  Memo2.Text :=('False');
end;

procedure TForm1.Button4Click(Sender: TObject);
var a, b : string;
begin
a := Memo1.Text;
b := a.Trim;
Memo2.lines.Add(b);
end;

procedure TForm1.Button5Click(Sender: TObject);
var c1 : string;
begin
c1 := AnsiLowerCase(Memo1.Text);
Memo2.Text := (c1);

end;

procedure TForm1.Button6Click(Sender: TObject);
var c2 : string;
begin
c2 := AnsiUpperCase	(Memo1.Text);
Memo2.Text := (c2);
end;

procedure TForm1.Button7Click(Sender: TObject);

begin
Memo2.Text := StringReplace((Memo1.Text),'s','t',[rfReplaceAll]);
end;
end.
