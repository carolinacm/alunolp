unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Vcl.ExtCtrls, Vcl.Imaging.jpeg;

type
  TForm1 = class(TForm)
    listaherois: TListBox;
    campoadicionar: TEdit;
    enviarheroi: TButton;
    apagarheroi: TButton;
    apagartdherois: TButton;
    atualizarheroi: TButton;
    imagemhqs: TImage;
    paineladicionar: TPanel;
    salvarheroi: TButton;
    procedure enviarheroiClick(Sender: TObject);
    procedure apagarheroiClick(Sender: TObject);
    procedure apagartdheroisClick(Sender: TObject);
    procedure atualizarheroiClick(Sender: TObject);
    procedure listaheroisClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.enviarheroiClick(Sender: TObject);
begin
listaherois.Items.Add(campoadicionar.Text);

end;

procedure TForm1.listaheroisClick(Sender: TObject);
begin
  campoadicionar.Text := listaherois.Items[listaherois.ItemIndex];
end;

procedure TForm1.apagarheroiClick(Sender: TObject);
begin
listaherois.Items.Delete(listaherois.itemindex);
end;

procedure TForm1.apagartdheroisClick(Sender: TObject);
begin
listaherois.Items.Clear;
end;

procedure TForm1.atualizarheroiClick(Sender: TObject);
begin
   listaherois.Items[listaherois.ItemIndex] := campoadicionar.Text;
end;

end.
