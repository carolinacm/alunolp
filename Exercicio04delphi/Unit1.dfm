object Form1: TForm1
  Left = 192
  Top = 125
  Caption = 'Form1'
  ClientHeight = 406
  ClientWidth = 513
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 80
    Top = 16
    Width = 298
    Height = 29
    Caption = 'Adicione um personagem'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lista: TListBox
    Left = 224
    Top = 64
    Width = 201
    Height = 249
    ItemHeight = 13
    TabOrder = 0
    OnClick = listaClick
  end
  object campo: TEdit
    Left = 16
    Top = 64
    Width = 169
    Height = 21
    TabOrder = 1
  end
  object enviar: TButton
    Left = 16
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Enviar'
    TabOrder = 2
    OnClick = enviarClick
  end
  object apagar: TButton
    Left = 16
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Apagar'
    TabOrder = 3
    OnClick = apagarClick
  end
  object apagartd: TButton
    Left = 16
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Apagar tudo'
    TabOrder = 4
    OnClick = apagartdClick
  end
  object botaoeditar: TButton
    Left = 16
    Top = 256
    Width = 75
    Height = 25
    Caption = 'Atualizar'
    TabOrder = 5
    OnClick = botaoeditarClick
  end
end
