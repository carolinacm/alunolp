unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    lista: TListBox;
    campo: TEdit;
    enviar: TButton;
    Label1: TLabel;
    apagar: TButton;
    apagartd: TButton;
    botaoeditar: TButton;
    procedure enviarClick(Sender: TObject);
    procedure apagarClick(Sender: TObject);
    procedure apagartdClick(Sender: TObject);
    procedure botaoeditarClick(Sender: TObject);
    procedure listaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.enviarClick(Sender: TObject);
begin
lista.Items.Add(campo.Text);

end;

procedure TForm1.listaClick(Sender: TObject);
begin
  campo.Text := lista.Items[lista.ItemIndex];
end;

procedure TForm1.apagarClick(Sender: TObject);
begin
lista.Items.Delete(lista.itemindex);
end;

procedure TForm1.apagartdClick(Sender: TObject);
begin
lista.Items.Clear;
end;

procedure TForm1.botaoeditarClick(Sender: TObject);
begin
   lista.Items[lista.ItemIndex] := campo.Text;
end;

end.
